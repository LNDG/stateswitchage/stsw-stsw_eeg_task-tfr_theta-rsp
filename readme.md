[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Theta power at response

HDDM models suggest threshold changes, which in the past have been associated with changes in mediofrontal theta power during the decision process. Here, we test this assumption.

## Function overview

**a01_calculate_theta_power**

- calculate theta power for individual subjects
- CSD transform
- 3-cycle wavelet (2 to 8 Hz, -1s to +3.5s peri-stimulus)
- resegment to probe on- and offset
- create load conditions
- average across trials

**a02_theta_plot**

Topographies at response:
![image](figures/PeriRspThetaTopo.png)

Probe-locked theta:
![image](figures/ProbeRelatedTheta.png)

Response-locked theta:
![image](figures/RspRelatedTheta.png)

**a03_theta_CBPA**

- outdated script with cluster-based-permutation analyses