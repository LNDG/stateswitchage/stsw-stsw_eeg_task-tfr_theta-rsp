% check mu-beta dynamics as an indicator of motor preparation

%% initialize

clear all; close all; pack; clc;
restoredefaultpath
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% collect all subjects

ERPstruct = [];
for id = 1:length(IDs)
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/M_RespTheta/';
    load([pn.out, IDs{id}, '_motor_v5.mat'], 'dataResponseAvg_Theta', 'dataResponseAvg_Theta_Stim');
    dataStimAvg_Theta = dataResponseAvg_Theta_Stim; clear dataResponseAvg_Theta_Stim;
    freqs = dataResponseAvg_Theta{1,1}.freq > 1.95 & dataResponseAvg_Theta{1,1}.freq <= 8;
    % average across alpha-beta freqs
    for indCond = 1:4
        dataStimAvg_Theta{indCond}.avg = squeeze(nanmean(dataStimAvg_Theta{indCond}.powspctrm(:,freqs,:),2));
        dataStimAvg_Theta{indCond}.dimord = 'chan_time';
        dataStimAvg_Theta{indCond} = ...
            rmfield(dataStimAvg_Theta{indCond}, 'powspctrm');
        dataStimAvg_Theta{indCond} = ...
            rmfield(dataStimAvg_Theta{indCond}, 'freq');
        
        dataResponseAvg_Theta{indCond}.avg = squeeze(nanmean(dataResponseAvg_Theta{indCond}.powspctrm(:,freqs,:),2));
        dataResponseAvg_Theta{indCond}.dimord = 'chan_time';
        dataResponseAvg_Theta{indCond} = ...
            rmfield(dataResponseAvg_Theta{indCond}, 'powspctrm');
        dataResponseAvg_Theta{indCond} = ...
            rmfield(dataResponseAvg_Theta{indCond}, 'freq');
    end
    ERPstruct.dataStimAvg_Theta(:,id) = dataStimAvg_Theta;
    ERPstruct.dataResponseAvg_Theta(:,id) = dataResponseAvg_Theta;
end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1%:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        dataStimAvg_Theta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg_Theta{indCond,ageIdx{indAge}});
        dataResponseAvg_Theta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg_Theta{indCond,ageIdx{indAge}});
    end
end

%% baseline correction: -200:0 w.r.t. probe onset on grand average beta series (condition-specific!)
% always normalize by pre-probe values

dataResponseAvg_AlphaBeta_BL = [];
idxBL = dataStimAvg_Theta{1,1}.time >2.8 & dataStimAvg_Theta{1,1}.time<3; % use time indices of probe
for indAge = 1%:2
    for indCond = 1:4
        numTime = size(dataResponseAvg_Theta{indCond,indAge}.individual,3);
        dataResponseAvg_AlphaBeta_BL{indCond,indAge} = dataResponseAvg_Theta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataStimAvg_Theta{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numTime);
        numTime = size(dataStimAvg_Theta{indCond,indAge}.individual,3);
        dataStimAvg_AlphaBeta_BL{indCond,indAge} = dataStimAvg_Theta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataStimAvg_Theta{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numTime);
    end
end

%% CBPA: linear probe-related theta increases

    CBPAstruct = [];
    indGroup = 1;
    for indCond = 1:4
        CBPAstruct{indCond} = dataStimAvg_Theta{indCond,indGroup};
        CBPAstruct{indCond}.dimord = 'subj_chan_time';
        CBPAstruct{indCond}.time = CBPAstruct{indCond}.time;
        CBPAstruct{indCond}.individual = dataStimAvg_AlphaBeta_BL{indCond,indGroup};
    end
    
    cfg = [];
    cfg.method           = 'montecarlo';
    cfg.statistic        = 'ft_statfun_depsamplesregrT';
    cfg.channel          = [CBPAstruct{1}.label(10:12)];
    cfg.avgoverchan      = 'yes';
    cfg.latency          = [6 8];
    cfg.avgovertime      = 'no';
    cfg.correctm         = 'cluster';
    cfg.clusteralpha     = 0.05;
    cfg.clusterstatistic = 'maxsum';
    cfg.tail             = 1;
    cfg.clustertail      = 1;
    cfg.alpha            = 0.05;
    cfg.numrandomization = 1000; % number of iterations

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = CBPAstruct{1}.label;
    cfg.neighbours          = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1}); % use original dataset
    clear cfg_neighb;

    % specify design
    subj = size(CBPAstruct{1}.individual,1);
    conds = 4;
    design = zeros(2,conds*subj);
    for i = 1:subj
        for icond = 1:conds
            design(1,(icond-1)*subj+i) = i;
        end
    end
    for icond = 1:conds
        design(2,(icond-1)*subj+1:icond*subj) = icond;
    end

    cfg.design = design; clear design subj;
    cfg.uvar  = 1;
    cfg.ivar  = 2;
    cfg.parameter = 'individual';

    [stat] = ft_timelockstatistics(cfg, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});
    
    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/M_RespTheta/Z_CBPA_linear.mat'], 'stat', 'cfg')
    
%% plot probe-locked theta

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/M_RespTheta/Z_CBPA_linear.mat'], 'stat', 'cfg')

pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
         
    colorm = [8.*[.12 .1 .1]; 6.*[.15 .1 .1]; 4.*[.2 .1 .1]; 2.*[.3 .1 .1]];

    h = figure('units','normalized','position',[.1 .1 .2 .2]); cla;
    % between-subject error bars
    % rearrange data in matrix
    BLTheta = [];
    for indCond = 1:4
        BLTheta(indCond,:,:) = squeeze(nanmean(dataStimAvg_AlphaBeta_BL{indCond,1}(:,[10:12],:),2)); % 10:12
    end
    cla; hold on;
    % between-subject error bars
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(BLTheta,1));
    curData = squeeze(BLTheta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(1,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(2,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(3,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(4,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(4,:),'linewidth', 2}, 'patchSaturation', .1);
    xlim([5.8 8]-6); xlabel('Time from probe onset (s)')
    line([6 6]-6, get(gca, 'Ylim'),'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'location', 'NorthEast')
    legend('boxoff');
    ylim([-.03, .22])
    ylabel({'Theta power (2-8 Hz)';'(normalized; a.u.)'});
    title({'Probe-related frontal theta power';'increases with target load'})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    significantVals = double(stat.mask);
    significantVals(significantVals==0) = NaN;
    plot(stat.time-6, significantVals.*-.01, 'k', 'LineWidth', 4)
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/C_figures/M_probeTheta/';
figureName = 'ProbeRelatedThetaIncrease';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% linear response-related theta changes

    CBPAstruct = [];
    indGroup = 1;
    for indCond = 1:4
        CBPAstruct{indCond} = dataStimAvg_Theta{indCond,indGroup};
        CBPAstruct{indCond}.dimord = 'subj_chan_time';
        CBPAstruct{indCond}.time = dataResponseAvg_Theta{indCond,indGroup}.time;
        CBPAstruct{indCond}.individual = dataResponseAvg_AlphaBeta_BL{indCond,indGroup};
    end
    
    cfg = [];
    cfg.method           = 'montecarlo';
    cfg.statistic        = 'ft_statfun_depsamplesregrT';
    cfg.channel          = [CBPAstruct{1}.label(10:12)];
    cfg.avgoverchan      = 'yes';
    cfg.correctm         = 'cluster';
    cfg.clusteralpha     = 0.05;
    cfg.clusterstatistic = 'maxsum';
    cfg.tail             = 1;
    cfg.clustertail      = 1;
    cfg.alpha            = 0.05;
    cfg.numrandomization = 1000; % number of iterations

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = CBPAstruct{1}.label;
    cfg.neighbours          = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1}); % use original dataset
    clear cfg_neighb;

    % specify design
    subj = size(CBPAstruct{1}.individual,1);
    conds = 4;
    design = zeros(2,conds*subj);
    for i = 1:subj
        for icond = 1:conds
            design(1,(icond-1)*subj+i) = i;
        end
    end
    for icond = 1:conds
        design(2,(icond-1)*subj+1:icond*subj) = icond;
    end

    cfg.design = design; clear design subj;
    cfg.uvar  = 1;
    cfg.ivar  = 2;
    cfg.parameter = 'individual';

    [stat] = ft_timelockstatistics(cfg, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});
    
    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/M_RespTheta/Z_CBPA_resp_linear.mat'], 'stat', 'cfg')
    
%% plot response-locked theta
    
    h = figure('units','normalized','position',[.1 .1 .2 .2]); cla;
    BLTheta = [];
    for indCond = 1:4
        BLTheta(indCond,:,:) = squeeze(nanmean(dataResponseAvg_AlphaBeta_BL{indCond,1}(:,[10:12],:),2)); % 10:12
    end
    cla; hold on;
    % between-subject error bars
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(BLTheta,1));
    curData = squeeze(BLTheta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(1,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(2,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(3,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(4,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(4,:),'linewidth', 2}, 'patchSaturation', .1);
    xlim([-800 200]); xlabel('Time (ms); resp-locked')
    ylim([-.03, .22])
    line([0 0], get(gca, 'Ylim'),'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
    ylabel({'Theta power (2-8 Hz)';'(normalized; a.u.)'});
    title({'Response-related frontal theta power';'increases with target load'})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    significantVals = double(stat.mask);
    significantVals(significantVals==0) = NaN;
    plot(stat.time-6, significantVals.*-.015, 'k', 'LineWidth', 4)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/C_figures/M_probeTheta/';
    figureName = 'ResponseRelatedThetaIncrease';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    %% export theta averags across final 50 ms for follow-up analyses
    
    timeIdx = dataResponseAvg_Theta{1,1}.time>=-50 & dataResponseAvg_Theta{1,1}.time <=0;
    
    thetaPow_PreResp = [];
    for indCond = 1:4
        thetaPow_PreResp(:,indCond) = squeeze(nanmean(nanmean(dataResponseAvg_AlphaBeta_BL{indCond,1}(:,[10:12],timeIdx),2),3)); % 10:12
    end
     
    ThetaResp.data = thetaPow_PreResp;
    ThetaResp.IDs = IDs;

    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/Z_ThetaResp_YA.mat'], 'ThetaResp');
