% check mu-beta dynamics as an indicator of motor preparation

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;
pn.data = fullfile(pn.root, 'data');
pn.tools = fullfile(pn.root, 'tools');
pn.plotFolder = fullfile(pn.root, 'figures');

addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools,'shadedErrorBar'))
addpath(fullfile(pn.tools,'BrewerMap'))

%% define IDs

filename = fullfile(pn.root, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

channels_theta = 19:21;

%% collect all subjects

ERPstruct = [];
for id = 1:length(IDs)
    load(fullfile(pn.data, [IDs{id}, '_theta']),...
        'dataResponseAvg_Theta', 'dataResponseAvg_Theta_Stim');
    dataStimAvg_Theta = dataResponseAvg_Theta_Stim; clear dataResponseAvg_Theta_Stim;
    % average across theta freqs
    for indCond = 1:4
        cfg = [];
        cfg.avgoverfreq = 'yes';
        cfg.frequency = [1.95, 8];
        dataStimAvg_Theta{indCond} = ft_selectdata(cfg, dataStimAvg_Theta{indCond});
        dataResponseAvg_Theta{indCond} = ft_selectdata(cfg, dataResponseAvg_Theta{indCond});
    end
    ERPstruct.dataStimAvg_Theta(:,id) = dataStimAvg_Theta;
    ERPstruct.dataResponseAvg_Theta(:,id) = dataResponseAvg_Theta;
end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        cfg.param = 'powspctrm';
        curIdx = find(ageIdx{indAge});
        for indID = 1:numel(curIdx)
            dataStimAvg_Theta{indCond,indAge}.individual(indID,:,:) = ...
                squeeze(ERPstruct.dataStimAvg_Theta{indCond,curIdx(indID)}.powspctrm);
            dataResponseAvg_Theta{indCond,indAge}.individual(indID,:,:) = ...
                squeeze(ERPstruct.dataResponseAvg_Theta{indCond,curIdx(indID)}.powspctrm);
        end
    end
end

%% baseline correction: -200:0 w.r.t. probe onset on theta time series (condition-specific!)
% always normalize by pre-probe values

dataResponseAvg_BL = [];
idxBL = dataStimAvg_Theta{1,1}.time >2.8 & dataStimAvg_Theta{1,1}.time<3; % use time indices of probe
for indAge = 1:2
    for indCond = 1:4
        numTime = size(dataResponseAvg_Theta{indCond,indAge}.individual,3);
        dataResponseAvg_BL{indCond,indAge} = dataResponseAvg_Theta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataStimAvg_Theta{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numTime);
        numTime = size(dataStimAvg_Theta{indCond,indAge}.individual,3);
        dataStimAvg_BL{indCond,indAge} = dataStimAvg_Theta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataStimAvg_Theta{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numTime);
    end
end

%% Figure: plot overview of mediofrontal theta increases

% at posterior channels, we observe a strong locking of theta power to
% probe onset, and therefore smoothing to RT (Werkle-Bergner et al.)

% central theta should be locked to response
% It is indeed response-locked and decreases with load

colorm = [8.*[.12 .1 .1]; 6.*[.15 .1 .1]; 4.*[.2 .1 .1]; 2.*[.3 .1 .1]];

h = figure('units','centimeters','position',[0 0 40 10]);
set(gcf,'renderer','Painters')
for indAge = 1:2
    subplot(1,2,indAge); cla;
        % between-subject error bars
        % rearrange data in matrix
        BLTheta = [];
        for indCond = 1:4
            BLTheta(indCond,:,:) = squeeze(nanmean(dataResponseAvg_BL{indCond,indAge}(:,[channels_theta],:),2)); % channels_theta
        end
        cla; hold on;
        % between-subject error bars
        % new value = old value ? subject average + grand average
        condAvg = squeeze(nanmean(BLTheta,1));
        curData = squeeze(BLTheta(1,:,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(1,:),'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BLTheta(2,:,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(2,:),'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BLTheta(3,:,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(3,:),'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BLTheta(4,:,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar(dataResponseAvg_Theta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', colorm(4,:),'linewidth', 2}, 'patchSaturation', .1);
        xlim([-800 200]); ylim([-.1, .25]); xlabel('Time (ms); resp-locked')
        line([0 0], get(gca, 'Ylim'),'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
        legend([l1.mainLine, l4.mainLine], ...
            {'Load 1'; 'Load 4'}, 'location', 'NorthWest')
        legend('boxoff');
        ylabel({'Theta power';'(2-8 Hz) (a.u.)'});
end
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'RspRelatedTheta';

saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    
%% plot probe-locked theta
         
colorm = [8.*[.12 .1 .1]; 6.*[.15 .1 .1]; 4.*[.2 .1 .1]; 2.*[.3 .1 .1]];

h = figure('units','centimeters','position',[0 0 40 10]);
set(gcf,'renderer','Painters')
for indAge = 1:2
    subplot(1,2,indAge); cla;
    % between-subject error bars
    % rearrange data in matrix
    BLTheta = [];
    for indCond = 1:4
        BLTheta(indCond,:,:) = squeeze(nanmean(dataStimAvg_BL{indCond,indAge}(:,[channels_theta],:),2)); % channels_theta
    end
    cla; hold on;
    % between-subject error bars
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(BLTheta,1));
    curData = squeeze(BLTheta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(1,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(2,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(3,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLTheta(4,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataStimAvg_Theta{1,1}.time-6,nanmean(curData,1),standError, 'lineprops', {'color', colorm(4,:),'linewidth', 2}, 'patchSaturation', .1);
    xlim([5.8 8]-6); xlabel('Time from probe onset (s)')
    line([6 6]-6, get(gca, 'Ylim'),'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'location', 'NorthEast')
    legend('boxoff');
    ylim([-.1, .22])
    ylabel({'Theta power (2-8 Hz)';'(normalized; a.u.)'});
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
end

figureName = 'ProbeRelatedTheta';

saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% Figure: plot topography of theta around response

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-6*10^-6 6*10^-6];
%cfg.zlim = [-4*10^-6 4*10^-6];

cfg.marker = 'off';
cfg.highlight = 'on';
cfg.highlightcolor = [1 1 1];
cfg.highlightsymbol = '.';
cfg.highlightsize = 20;
cfg.highlightchannel = dataResponseAvg_Theta{1,1}.label([19:21]);

h = figure('units','centimeters','position',[0 0 20 10]);
for indAge = 1:2
    subplot(1,2,indAge)
    cfg.figure = h;
    plotData = [];
    plotData.label = dataResponseAvg_Theta{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseAvg_Theta{1,1}.time>-50 & dataResponseAvg_Theta{1,1}.time<50;
    % average across loads
    LoadAverageL = squeeze(nanmean(cat(4, dataResponseAvg_BL{:,indAge}),4));
    plotData.powspctrm = squeeze(nanmean(nanmean(LoadAverageL(:,:,idxTime),3),1))';
    % LoadAverageL = squeeze(dataResponseAvg_BL{4,1}-dataResponseAvg_BL{1,1});
    % plotData.powspctrm = squeeze(nanmean(nanmean(LoadAverageL(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','Theta power (a.u.)');
end
suptitle({'Theta: -50:50 ms peri-response'; ''})

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'PeriRspThetaTopo';

saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');