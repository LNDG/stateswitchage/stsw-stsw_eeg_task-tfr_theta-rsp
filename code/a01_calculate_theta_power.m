function a01_calculate_theta_power(id, rootpath)

    if ismac % run if function is not pre-compiled
        id = '1126'; % test for example subject
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..'))
        rootpath = pwd;
    end

    %% pathdef

    pn.dataIn       = fullfile(rootpath, '..', 'X1_preprocEEGData');
    pn.out          = fullfile(rootpath, 'data');
    pn.tools        = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

    %% define IDs for segmentation

    display(['processing ID ' id]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load(fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');

    %% save TrlInfo from FT

    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;

    %% CSD transform

    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);

    %% wavelet transform (1 cycle): theta motor

    cfg                 = [];
    cfg.channel         = 'all';
    cfg.method          = 'wavelet';
     cfg.width           = 3;
    cfg.keeptrials      = 'yes';
    cfg.output          = 'pow';
    cfg.foi             = 2:1:8;
    cfg.toi             = 2:0.05:9.5; % 20 Hz
    TFRdata_AlphaBeta   = ft_freqanalysis(cfg, data);

    %% re-segment data with respect to response 

    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(TFRdata_AlphaBeta.powspctrm,4));   
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-40; % now 20 Hz
    cfg.endsample = idx+20;
    trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(TFRdata_AlphaBeta.powspctrm,4));
    dataResponse_Theta = TFRdata_AlphaBeta;
    dataResponse_Theta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        if ismember(indTrial, trialToRemove)
            dataResponse_Theta.powspctrm(indTrial,:,:,:) = NaN;
        else
            dataResponse_Theta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse_Theta.time = [-40*.05:.05:20*.05].*1000; % in ms

    % remove empty trials from all sturctures

    TFRdata_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; TFRdata_AlphaBeta.trialToRemove = numel(trialToRemove);
    dataResponse_Theta.powspctrm(trialToRemove,:,:,:) = []; dataResponse_Theta.trialToRemove = numel(trialToRemove);

    TrlInfo(trialToRemove,:) = [];

    disp(['Removed trials: ', num2str(numel(trialToRemove))]);

    % average within condition

    for indCond = 1:4
        cfg = [];
        cfg.trials = TrlInfo(:,8)==indCond;
        % timelock to stim/probe
        dataResponseAvg_Theta_Stim{indCond} = TFRdata_AlphaBeta;
        dataResponseAvg_Theta_Stim{indCond}.powspctrm = ...
            squeeze(nanmean(log10(TFRdata_AlphaBeta.powspctrm(cfg.trials,:,:,:)),1));
        dataResponseAvg_Theta_Stim{indCond}.dimord = 'chan_freq_time';

        % timelock to response
        dataResponseAvg_Theta{indCond} = dataResponse_Theta;
        dataResponseAvg_Theta{indCond}.powspctrm = ...
            squeeze(nanmean(log10(dataResponseAvg_Theta{indCond}.powspctrm(cfg.trials,:,:,:)),1));
        dataResponseAvg_Theta{indCond}.dimord = 'chan_freq_time';
    end

    %% save computed files

    save(fullfile(pn.out, [id, '_theta.mat']), ...
        'dataResponseAvg_Theta_Stim', ...
        'dataResponseAvg_Theta');

    clear data*

end