#!/bin/bash

fun_name="a01_calculate_theta_power"
job_name="stsw_theta_resp"

mkdir ./logs

rootpath="$(pwd)/.."
rootpath=$(builtin cd $rootpath; pwd)

# path to the text file with all subject ids:
path_ids="${rootpath}/code/id_list.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

for subj in $IDS; do 
  	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 4 \
  		--mem 4gb \
  		--time 02:00:00 \
  		--output ./logs/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2016b; matlab -nodisplay -r \"${fun_name}('${subj}','${rootpath}')\""
done